#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Architecture/CLIElementBase/clielementbase.h"

#include "../../Interfaces/Utility/i_user_task_data_ext.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.ToDoListCommand" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();

	// PluginBase interface
protected:
	void onReady() override;

private slots:
	void handle(const QStringList& args);

private:
	void showTask(int itemId);
	ExtendableItemDataMap getAttributes(const QStringList& args, QPointer<IExtendableDataModel> model);

private:
	CLIElement* m_cliElement;
	ReferenceInstancePtr<IUserTaskDataExtention> m_taskManager;

	QPointer<IExtendableDataModelFilter> m_userTasksFilter;
};
